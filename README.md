WordPress Auth Driver for Laravel
=================================

Allows you to authenticate users against a user table from a WordPress installation

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

	:::json
    {
        "require": {
            "hampel/wordpress-auth-laravel": "1.1.*"
        }
    }

Run Composer to update the new requirement.

	:::bash
    $ composer update

Open your Laravel config file `app/config/app.php` and add the two service providers to the providers array:

	:::php
    'providers' => array(

        ...

		'Hampel\WordPress\Auth\WordPressAuthServiceProvider',
		'Hampel\WordPress\Hashing\WordPressHashServiceProvider',

    ),

The WordPressAuthServiceProvider is where the Auth service is extended with a new "wordpress" user provider.

The WordPressHashServiceProvider provides a new service "wordpress-hash" which hashes values using the same library that WordPress uses.

Open your Laravel config file `app/config/auth.php` and set the driver to `wordpress`:

	:::php
	'driver' => 'wordpress',

You must configure a database connection for the wordpress user provider so it can find the WordPress tables. By default, it looks
for the `mysql` connection, but you can override this by setting a configuration option.

To create a new database connection, edit or over-ride the settings in `app/config/database.php` (this does not need to be the
default connection!). For example, to create a new connection `mysql-wordpress`:

	:::php
	'mysql-wordpress' => array(
		'driver'    => 'mysql',
		'host'      => 'localhost',
		'database'  => 'wordpress',
		'username'  => '',
		'password'  => '',
		'charset'   => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix'    => 'wp_',
	),

Make sure the table prefix is set to the same value specified in your WordPress `wp-config.php` file, which by default would be:

	:::php
	$table_prefix  = 'wp_';

Even if you have your Laravel data in the same database as WordPress, if your WordPress uses a table prefix but your Laravel tables
don't, you should specify a new database connection in Laravel as per the above instructions, just so you can set the prefix.

Next, publish the package configuration files using the artisan command:

	:::bash
	php artisan config:publish hampel/wordpress-auth-laravel

Edit or over-ride the `connection` config option in `app/config/packages/hampel/wordpress-auth-laravel/auth.php` to set the new
database connection:

	:::php
	'connection' => 'mysql-wordpress',

There are other config values available to over-ride the default WordPress table name and column names, but these should not normally
need to be changed.

Usage
-----

Implement user authentication for your Laravel application as normal, following the instructions in [http://laravel.com/docs/security](http://laravel.com/docs/security).

There is a `Hampel\WordPress\Auth\WordPressUser` model provided to replace the default `User` model supplied by Laravel.

The user data to be passed to the `Auth::attempt` function must use `user_login` and `user_pass` to match the column names in
WordPress. For example:

	:::php
	$userdata = array(
		'user_login' => Input::get('username'),
		'user_pass' => Input::get('password')
	);

	if (Auth::attempt($userdata))
	{
		// successfully logged in
	}
	else
	{
		// authentication failed
	}

There are config values supplied for these column names (although they are unlikely to change), so you could also use something like:

	:::php
	$userdata = array(
		Config::get('wordpress-auth-laravel::auth.username') => Input::get('username'),
		Config::get('wordpress-auth-laravel::auth.password') => Input::get('password')
	);

Todo
----

* Could do with some more unit tests
* Password reminders - should we do them or leave it to WordPress?
* Facade for wordpress-hash service
* Might be useful to expose user-meta from WordPress
* While we're at it, how about exposing WordPress roles too?