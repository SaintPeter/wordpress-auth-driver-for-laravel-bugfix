<?php namespace Hampel\WordPress\Auth;

use Illuminate\Support\ServiceProvider;
use Illuminate\Auth\Guard;

class WordPressAuthServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('hampel/wordpress-auth-laravel', 'wordpress-auth-laravel', __DIR__ . '/../../..');

		$app = $this->app;

		$this->app['auth']->extend('wordpress', function($app)
		{
			$connection = $app['config']->get('wordpress-auth-laravel::auth.connection');
			$provider = new WordPressUserProvider($app['wordpress-hash'], 'Hampel\WordPress\Auth\WordPressUser', $connection);
			$provider->setContainer($app);

			return new Guard($provider, $app['session.store']);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}